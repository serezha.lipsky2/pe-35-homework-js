function showPassword(inputFieldId, eye){
    const password = document.getElementById(inputFieldId)
    if(password.type === "text") {password.type = "password"}
    else {password.type="text"}
    eye.classList.toggle('fa-eye-slash');
    eye.classList.toggle('fa-eye');
}
document.querySelector(".btn").addEventListener("click", ()=>{
    const input = document.getElementById("input")
    const confirm = document.getElementById("confirm")
    const badPass = document.querySelector(".bad-pass")
    if(input.value === confirm.value){
        badPass.innerText = ""
        setTimeout(function() {
            alert("You're Welcome!")
        },100)
    }else {
        badPass.innerText = "Passwords must match. Try again"
        badPass.style.color =  "red"
        badPass.style.marginBottom = "24px"
    }
})
