const images = document.querySelectorAll(".image-to-show");
const stop = document.querySelector(".stop");
const start = document.querySelector(".start");
const timer = document.querySelector(".timer");
let second = +(3.0).toFixed(2);
let position = 1;

let imgTimerID = setInterval(showImg, 3000);
let stopWatchID = setInterval(stopWatch, 10);

function showImg() {
  images.forEach((element, index) => {
    element.classList.add("hidden");
    element.classList.remove("show");
    if (index === position) {
      element.classList.remove("hidden");
      element.classList.add("show");
    }
  });
  if (position < images.length - 1) {
    position++;
  } else position = 0;
}

function stopWatch() {
  timer.innerHTML = second.toFixed(2);
  if (second < 0) {
    second = +(3.0).toFixed(2);
  }
  second -= 0.01;
}

stop.addEventListener("click", (e) => {
  clearInterval(imgTimerID);
  clearInterval(stopWatchID);
});

start.addEventListener("click", (e) => {
  imgTimerID = setInterval(showImg, 3000);
  stopWatchID = setInterval(stopWatch, 10);
});
