let changeBtn = document.getElementById("theme-button");
let themeLink = document.getElementById("link");

if (localStorage.getItem('theme') === 'dark') {
	themeLink.setAttribute('href', "./css/dark.css")
}
else if (localStorage.getItem('theme') === 'light') {
	themeLink.setAttribute('href', "./css/light.css")
}

changeBtn.addEventListener("click", ChangeTheme);

function ChangeTheme() {
	let lightTheme = "./css/light.css";
	let darkTheme = "./css/dark.css";
	let currTheme = themeLink.getAttribute("href");

	if (currTheme == lightTheme) {
		currTheme = darkTheme;
		window.localStorage.setItem('theme', 'dark');
	}
	else {
		currTheme = lightTheme;
		window.localStorage.setItem('theme', 'light');
	}

	themeLink.setAttribute("href", currTheme);
}
