jQuery(document).ready(function () {
	let btn = $('#button');
	$(window).scroll(function () {
		if ($(window).scrollTop() > 300) {
			btn.addClass('show');
		} else {
			btn.removeClass('show');
		}
	});
	btn.on('click', function (e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: 0 }, '300');
	});
});
$(document).ready(function () {
	$("#menu").on("click", "a", function (event) {
		event.preventDefault();
		let id = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({ scrollTop: top }, 1500);
	});
});
$(document).ready(function () {
	$('.toggle').click(function () {
		$('.posts').slideToggle(300, function () {
			if ($(this).is(':hidden')) {
				$('.toggle').html('Показать Секцию');
			} else {
				$('.toggle').html('Скрыть Секцию');
			}
		});
		return false;
	});
});