"use strict";

function itemList (array, parent = document.body){
	const newArray = array.map(element => {
		let town = element.split("-").toString().toUpperCase();
		let list = document.createElement("p");
		list.innerHTML = `<strong>${town}</strong>`;
		list.className = "items-list";
		parent.append(list);
	});
	return newArray;
};
itemList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);